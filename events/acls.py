from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state, null=True):
    # Create a dictionary for the headers to use in the request
    headers = {'Authorization': PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = f"https://api.pexels.com/v1/search?query={city},{state}&per_page=1"
    # Make the request
    r = requests.get(url, headers=headers)
    # Parse the JSON response
    response = json.loads(r.content)
    # Return a dictionary that contains a `picture_url` key and
       #   one of the URLs for one of the pictures in the response
    return response["photos"]

# def get_weather_data(city, state, **kwargs):
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary111
    # Return the dictionary
    # pass
